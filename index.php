<?php

    session_start();

    

    if( isset($_GET['controlador']) && isset($_GET['funcion']) )
    {
        $controlador = $_GET['controlador'];
        $funcion = $_GET['funcion'];
        //include('config/conexionDB.php');
        include('Controladores/'.$controlador.'.php');
        $core = new $controlador();
        if(!method_exists($core, $funcion))
        {
            include('Controladores/Inicio.php');
            $core = new Inicio();
            $core->index();
        }
        $core->$funcion();
    } else 
    {
        include('Controladores/Inicio.php');
        $core = new Inicio();
        $core->index();
    }

?>