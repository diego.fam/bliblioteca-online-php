-- MySQL Script generated by MySQL Workbench
-- Thu Jan 17 15:36:28 2019
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema biblioteca
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema biblioteca
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `biblioteca` DEFAULT CHARACTER SET utf8 ;
USE `biblioteca` ;

-- -----------------------------------------------------
-- Table `biblioteca`.`tbl_tipo_usuarios`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `biblioteca`.`tbl_tipo_usuarios` (
  `idTipoUsuario` INT NOT NULL AUTO_INCREMENT,
  `nombreTipoUsuario` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`idTipoUsuario`),
  UNIQUE INDEX `idtbl_tipo_usuarios_UNIQUE` (`idTipoUsuario` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `biblioteca`.`tbl_pais`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `biblioteca`.`tbl_pais` (
  `idPais` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`idPais`),
  UNIQUE INDEX `idPais_UNIQUE` (`idPais` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `biblioteca`.`tbl_ciudad`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `biblioteca`.`tbl_ciudad` (
  `idCiudad` INT NOT NULL AUTO_INCREMENT,
  `idPais` INT NOT NULL,
  `nombre` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`idCiudad`),
  UNIQUE INDEX `idtbl_ciudad_UNIQUE` (`idCiudad` ASC) VISIBLE,
  INDEX `fk_tbl_ciudad_tbl_pais1_idx` (`idPais` ASC) VISIBLE,
  CONSTRAINT `fk_tbl_ciudad_tbl_pais1`
    FOREIGN KEY (`idPais`)
    REFERENCES `biblioteca`.`tbl_pais` (`idPais`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `biblioteca`.`tbl_personas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `biblioteca`.`tbl_personas` (
  `idPersona` INT NOT NULL AUTO_INCREMENT,
  `idCiudad` INT NOT NULL,
  `nombre` VARCHAR(50) NOT NULL,
  `apellidoPaterno` VARCHAR(50) NOT NULL,
  `apellidoMaterno` VARCHAR(50) NOT NULL,
  `correoElectronico` VARCHAR(70) NOT NULL,
  `cedula` VARCHAR(13) NOT NULL,
  `direccion` VARCHAR(100) NOT NULL,
  `estado` TINYINT NOT NULL,
  PRIMARY KEY (`idPersona`),
  UNIQUE INDEX `idtbl_personas_UNIQUE` (`idPersona` ASC) VISIBLE,
  INDEX `fk_tbl_personas_tbl_ciudad1_idx` (`idCiudad` ASC) VISIBLE,
  CONSTRAINT `fk_tbl_personas_tbl_ciudad1`
    FOREIGN KEY (`idCiudad`)
    REFERENCES `biblioteca`.`tbl_ciudad` (`idCiudad`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `biblioteca`.`tbl_usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `biblioteca`.`tbl_usuario` (
  `idUsuario` INT NOT NULL AUTO_INCREMENT,
  `idTipoUsuario` INT NOT NULL,
  `idPersona` INT NOT NULL,
  `fechaCreacion` DATE NOT NULL,
  `userName` VARCHAR(15) NOT NULL,
  `password` VARCHAR(42) NOT NULL,
  `estado` TINYINT NOT NULL,
  PRIMARY KEY (`idUsuario`),
  UNIQUE INDEX `idUsuario_UNIQUE` (`idUsuario` ASC) VISIBLE,
  INDEX `fk_tbl_usuario_tbl_tipo_usuarios_idx` (`idTipoUsuario` ASC) VISIBLE,
  INDEX `fk_tbl_usuario_tbl_personas1_idx` (`idPersona` ASC) VISIBLE,
  UNIQUE INDEX `userName_UNIQUE` (`userName` ASC) VISIBLE,
  CONSTRAINT `fk_tbl_usuario_tbl_tipo_usuarios`
    FOREIGN KEY (`idTipoUsuario`)
    REFERENCES `biblioteca`.`tbl_tipo_usuarios` (`idTipoUsuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_usuario_tbl_personas1`
    FOREIGN KEY (`idPersona`)
    REFERENCES `biblioteca`.`tbl_personas` (`idPersona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `biblioteca`.`tbl_autores`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `biblioteca`.`tbl_autores` (
  `idAutor` INT NOT NULL AUTO_INCREMENT,
  `idPersona` INT NOT NULL,
  `fechaCreacion` DATE NOT NULL,
  `estado` TINYINT NOT NULL,
  PRIMARY KEY (`idAutor`),
  UNIQUE INDEX `idtbl_autores_UNIQUE` (`idAutor` ASC) VISIBLE,
  INDEX `fk_tbl_autores_tbl_personas1_idx` (`idPersona` ASC) VISIBLE,
  CONSTRAINT `fk_tbl_autores_tbl_personas1`
    FOREIGN KEY (`idPersona`)
    REFERENCES `biblioteca`.`tbl_personas` (`idPersona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `biblioteca`.`tbl_editorial`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `biblioteca`.`tbl_editorial` (
  `idEditorial` INT NOT NULL AUTO_INCREMENT,
  `idCiudad` INT NOT NULL,
  `correoElectronico` VARCHAR(70) NOT NULL,
  `nombre` VARCHAR(50) NOT NULL,
  `estado` TINYINT NOT NULL,
  PRIMARY KEY (`idEditorial`),
  UNIQUE INDEX `idtbl_editorial_UNIQUE` (`idEditorial` ASC) VISIBLE,
  INDEX `fk_tbl_editorial_tbl_ciudad1_idx` (`idCiudad` ASC) VISIBLE,
  CONSTRAINT `fk_tbl_editorial_tbl_ciudad1`
    FOREIGN KEY (`idCiudad`)
    REFERENCES `biblioteca`.`tbl_ciudad` (`idCiudad`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `biblioteca`.`tbl_proveedor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `biblioteca`.`tbl_proveedor` (
  `idProveedor` INT NOT NULL AUTO_INCREMENT,
  `idCiudad` INT NOT NULL,
  `nombre` VARCHAR(50) NOT NULL,
  `direccion` VARCHAR(100) NOT NULL,
  `estado` TINYINT NOT NULL,
  PRIMARY KEY (`idProveedor`),
  UNIQUE INDEX `idtbl_proveedor_UNIQUE` (`idProveedor` ASC) VISIBLE,
  INDEX `fk_tbl_proveedor_tbl_ciudad1_idx` (`idCiudad` ASC) VISIBLE,
  CONSTRAINT `fk_tbl_proveedor_tbl_ciudad1`
    FOREIGN KEY (`idCiudad`)
    REFERENCES `biblioteca`.`tbl_ciudad` (`idCiudad`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `biblioteca`.`tbl_categoria_libro`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `biblioteca`.`tbl_categoria_libro` (
  `idCategoriaLibro` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`idCategoriaLibro`),
  UNIQUE INDEX `idtbl_categoria_libro_UNIQUE` (`idCategoriaLibro` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `biblioteca`.`tbl_tipoLibro`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `biblioteca`.`tbl_tipoLibro` (
  `idTipoLibro` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`idTipoLibro`),
  UNIQUE INDEX `idtbl_tipoLibro_UNIQUE` (`idTipoLibro` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `biblioteca`.`tbl_pasillo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `biblioteca`.`tbl_pasillo` (
  `idPasillo` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`idPasillo`),
  UNIQUE INDEX `idtbl_pasillo_UNIQUE` (`idPasillo` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `biblioteca`.`tbl_sectorEstante`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `biblioteca`.`tbl_sectorEstante` (
  `idSectorEstante` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`idSectorEstante`),
  UNIQUE INDEX `idtbl_ubicacionLibro_UNIQUE` (`idSectorEstante` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `biblioteca`.`tbl_estante`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `biblioteca`.`tbl_estante` (
  `idEstante` INT NOT NULL AUTO_INCREMENT,
  `idPasillo` INT NOT NULL,
  `idSectorEstante` INT NOT NULL,
  `nombre` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`idEstante`),
  UNIQUE INDEX `idtbl_estante_UNIQUE` (`idEstante` ASC) VISIBLE,
  INDEX `fk_tbl_estante_tbl_pasillo1_idx` (`idPasillo` ASC) VISIBLE,
  INDEX `fk_tbl_estante_tbl_sectorEstante1_idx` (`idSectorEstante` ASC) VISIBLE,
  CONSTRAINT `fk_tbl_estante_tbl_pasillo1`
    FOREIGN KEY (`idPasillo`)
    REFERENCES `biblioteca`.`tbl_pasillo` (`idPasillo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_estante_tbl_sectorEstante1`
    FOREIGN KEY (`idSectorEstante`)
    REFERENCES `biblioteca`.`tbl_sectorEstante` (`idSectorEstante`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `biblioteca`.`tbl_libros`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `biblioteca`.`tbl_libros` (
  `idLibro` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `idCategoriaLibro` INT NOT NULL,
  `idAutor` INT NOT NULL,
  `idEditorial` INT NOT NULL,
  `idProveedor` INT NOT NULL,
  `idTipoLibro` INT NOT NULL,
  `idEstante` INT NOT NULL,
  `nombre` VARCHAR(100) NOT NULL,
  `codigo` VARCHAR(50) NOT NULL,
  `stockPrestamo` INT(12) NULL,
  `stockVenta` INT(12) NULL,
  `valorPrestano` INT NOT NULL,
  `valorVenta` INT NOT NULL,
  `resumen` LONGTEXT NOT NULL,
  `estado` TINYINT NOT NULL,
  `imagen` BLOB NULL,
  `fechaIngreso` DATE NOT NULL,
  `descripcion` LONGTEXT NOT NULL,
  PRIMARY KEY (`idLibro`),
  INDEX `fk_tbl_libros_tbl_categoria_libro1_idx` (`idCategoriaLibro` ASC) VISIBLE,
  INDEX `fk_tbl_libros_tbl_autores1_idx` (`idAutor` ASC) VISIBLE,
  INDEX `fk_tbl_libros_tbl_editorial1_idx` (`idEditorial` ASC) VISIBLE,
  INDEX `fk_tbl_libros_tbl_proveedor1_idx` (`idProveedor` ASC) VISIBLE,
  INDEX `fk_tbl_libros_tbl_tipoLibro1_idx` (`idTipoLibro` ASC) VISIBLE,
  INDEX `fk_tbl_libros_tbl_estante1_idx` (`idEstante` ASC) VISIBLE,
  CONSTRAINT `fk_tbl_libros_tbl_categoria_libro1`
    FOREIGN KEY (`idCategoriaLibro`)
    REFERENCES `biblioteca`.`tbl_categoria_libro` (`idCategoriaLibro`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_libros_tbl_autores1`
    FOREIGN KEY (`idAutor`)
    REFERENCES `biblioteca`.`tbl_autores` (`idAutor`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_libros_tbl_editorial1`
    FOREIGN KEY (`idEditorial`)
    REFERENCES `biblioteca`.`tbl_editorial` (`idEditorial`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_libros_tbl_proveedor1`
    FOREIGN KEY (`idProveedor`)
    REFERENCES `biblioteca`.`tbl_proveedor` (`idProveedor`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_libros_tbl_tipoLibro1`
    FOREIGN KEY (`idTipoLibro`)
    REFERENCES `biblioteca`.`tbl_tipoLibro` (`idTipoLibro`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_libros_tbl_estante1`
    FOREIGN KEY (`idEstante`)
    REFERENCES `biblioteca`.`tbl_estante` (`idEstante`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `biblioteca`.`tbl_cliente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `biblioteca`.`tbl_cliente` (
  `idCliente` INT NOT NULL AUTO_INCREMENT,
  `idPersona` INT NULL,
  `idUsuario` INT NULL,
  `fechaCreacion` DATE NOT NULL,
  `estado` TINYINT NOT NULL,
  PRIMARY KEY (`idCliente`),
  INDEX `fk_tbl_cliente_tbl_personas1_idx` (`idPersona` ASC) VISIBLE,
  INDEX `fk_tbl_cliente_tbl_usuario1_idx` (`idUsuario` ASC) VISIBLE,
  UNIQUE INDEX `idCliente_UNIQUE` (`idCliente` ASC) VISIBLE,
  CONSTRAINT `fk_tbl_cliente_tbl_personas1`
    FOREIGN KEY (`idPersona`)
    REFERENCES `biblioteca`.`tbl_personas` (`idPersona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_cliente_tbl_usuario1`
    FOREIGN KEY (`idUsuario`)
    REFERENCES `biblioteca`.`tbl_usuario` (`idUsuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `biblioteca`.`tbl_comprobanteVenta`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `biblioteca`.`tbl_comprobanteVenta` (
  `idComprobanteVenta` INT NOT NULL AUTO_INCREMENT,
  `idCliente` INT NOT NULL,
  `fecha` DATE NOT NULL,
  `estado` TINYINT NOT NULL,
  PRIMARY KEY (`idComprobanteVenta`),
  UNIQUE INDEX `idtbl_comprobanteCompra_UNIQUE` (`idComprobanteVenta` ASC) VISIBLE,
  INDEX `fk_tbl_comprobanteCompra_tbl_cliente1_idx` (`idCliente` ASC) VISIBLE,
  CONSTRAINT `fk_tbl_comprobanteCompra_tbl_cliente1`
    FOREIGN KEY (`idCliente`)
    REFERENCES `biblioteca`.`tbl_cliente` (`idCliente`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `biblioteca`.`tbl_tipoPago`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `biblioteca`.`tbl_tipoPago` (
  `idTipoPago` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `detalle` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`idTipoPago`),
  UNIQUE INDEX `idtbl_tipopago_UNIQUE` (`idTipoPago` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `biblioteca`.`tbl_descuento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `biblioteca`.`tbl_descuento` (
  `idDescuento` INT NOT NULL AUTO_INCREMENT,
  `detalle` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`idDescuento`),
  UNIQUE INDEX `idtbl_descuento_UNIQUE` (`idDescuento` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `biblioteca`.`tbl_facturacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `biblioteca`.`tbl_facturacion` (
  `idFacturacion` INT NOT NULL AUTO_INCREMENT,
  `electronico` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idFacturacion`),
  UNIQUE INDEX `idtbl_facturacion_UNIQUE` (`idFacturacion` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `biblioteca`.`tbl_pago`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `biblioteca`.`tbl_pago` (
  `idPago` INT NOT NULL AUTO_INCREMENT,
  `idTipoPago` INT UNSIGNED NOT NULL,
  `idDescuento` INT NOT NULL,
  `idFacturacion` INT NOT NULL,
  `idComprobanteVenta` INT NOT NULL,
  `fecha` DATE NOT NULL,
  `neto` INT NOT NULL,
  `total` INT NOT NULL,
  PRIMARY KEY (`idPago`),
  UNIQUE INDEX `idtbl_pago_UNIQUE` (`idPago` ASC) VISIBLE,
  INDEX `fk_tbl_pago_tbl_tipoPago1_idx` (`idTipoPago` ASC) VISIBLE,
  INDEX `fk_tbl_pago_tbl_descuento1_idx` (`idDescuento` ASC) VISIBLE,
  INDEX `fk_tbl_pago_tbl_facturacion1_idx` (`idFacturacion` ASC) VISIBLE,
  INDEX `fk_tbl_pago_tbl_comprobanteVenta1_idx` (`idComprobanteVenta` ASC) VISIBLE,
  CONSTRAINT `fk_tbl_pago_tbl_tipoPago1`
    FOREIGN KEY (`idTipoPago`)
    REFERENCES `biblioteca`.`tbl_tipoPago` (`idTipoPago`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_pago_tbl_descuento1`
    FOREIGN KEY (`idDescuento`)
    REFERENCES `biblioteca`.`tbl_descuento` (`idDescuento`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_pago_tbl_facturacion1`
    FOREIGN KEY (`idFacturacion`)
    REFERENCES `biblioteca`.`tbl_facturacion` (`idFacturacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_pago_tbl_comprobanteVenta1`
    FOREIGN KEY (`idComprobanteVenta`)
    REFERENCES `biblioteca`.`tbl_comprobanteVenta` (`idComprobanteVenta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `biblioteca`.`tbl_comprobantePrestamo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `biblioteca`.`tbl_comprobantePrestamo` (
  `idComprobantePrestamo` INT NOT NULL AUTO_INCREMENT,
  `fecha` DATETIME NOT NULL,
  `estado` TINYINT NOT NULL,
  `tbl_cliente_idCliente` INT NOT NULL,
  PRIMARY KEY (`idComprobantePrestamo`),
  UNIQUE INDEX `idtbl_comprobantePrestamo_UNIQUE` (`idComprobantePrestamo` ASC) VISIBLE,
  INDEX `fk_tbl_comprobantePrestamo_tbl_cliente1_idx` (`tbl_cliente_idCliente` ASC) VISIBLE,
  CONSTRAINT `fk_tbl_comprobantePrestamo_tbl_cliente1`
    FOREIGN KEY (`tbl_cliente_idCliente`)
    REFERENCES `biblioteca`.`tbl_cliente` (`idCliente`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `biblioteca`.`tbl_librosVenta`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `biblioteca`.`tbl_librosVenta` (
  `idLibro` INT NOT NULL,
  `idComprobanteVenta` INT NOT NULL,
  PRIMARY KEY (`idLibro`, `idComprobanteVenta`),
  INDEX `fk_tbl_libros_has_tbl_comprobanteVenta_tbl_comprobanteVenta_idx` (`idComprobanteVenta` ASC) VISIBLE,
  INDEX `fk_tbl_libros_has_tbl_comprobanteVenta_tbl_libros1_idx` (`idLibro` ASC) VISIBLE,
  CONSTRAINT `fk_tbl_libros_has_tbl_comprobanteVenta_tbl_libros1`
    FOREIGN KEY (`idLibro`)
    REFERENCES `biblioteca`.`tbl_libros` (`idLibro`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_libros_has_tbl_comprobanteVenta_tbl_comprobanteVenta1`
    FOREIGN KEY (`idComprobanteVenta`)
    REFERENCES `biblioteca`.`tbl_comprobanteVenta` (`idComprobanteVenta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `biblioteca`.`tbl_librosPrestamo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `biblioteca`.`tbl_librosPrestamo` (
  `idComprobantePrestamo` INT NOT NULL,
  `idLibro` INT NOT NULL,
  PRIMARY KEY (`idComprobantePrestamo`, `idLibro`),
  INDEX `fk_tbl_comprobantePrestamo_has_tbl_libros_tbl_libros1_idx` (`idLibro` ASC) VISIBLE,
  INDEX `fk_tbl_comprobantePrestamo_has_tbl_libros_tbl_comprobantePr_idx` (`idComprobantePrestamo` ASC) VISIBLE,
  CONSTRAINT `fk_tbl_comprobantePrestamo_has_tbl_libros_tbl_comprobantePres1`
    FOREIGN KEY (`idComprobantePrestamo`)
    REFERENCES `biblioteca`.`tbl_comprobantePrestamo` (`idComprobantePrestamo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_comprobantePrestamo_has_tbl_libros_tbl_libros1`
    FOREIGN KEY (`idLibro`)
    REFERENCES `biblioteca`.`tbl_libros` (`idLibro`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `biblioteca`.`tbl_comprobanteDevolucion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `biblioteca`.`tbl_comprobanteDevolucion` (
  `idComprobanteDevolucion` INT NOT NULL AUTO_INCREMENT,
  `idComprobantePrestamo` INT NOT NULL,
  `fecha` DATETIME NOT NULL,
  `multa` INT NOT NULL,
  `estado` TINYINT NOT NULL,
  PRIMARY KEY (`idComprobanteDevolucion`),
  UNIQUE INDEX `idtbl_comprobanteDevolucion_UNIQUE` (`idComprobanteDevolucion` ASC) VISIBLE,
  INDEX `fk_tbl_comprobanteDevolucion_tbl_comprobantePrestamo1_idx` (`idComprobantePrestamo` ASC) VISIBLE,
  CONSTRAINT `fk_tbl_comprobanteDevolucion_tbl_comprobantePrestamo1`
    FOREIGN KEY (`idComprobantePrestamo`)
    REFERENCES `biblioteca`.`tbl_comprobantePrestamo` (`idComprobantePrestamo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `biblioteca`.`tbl_castigado`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `biblioteca`.`tbl_castigado` (
  `idCastigado` INT NOT NULL AUTO_INCREMENT,
  `idCliente` INT UNSIGNED NOT NULL,
  `fechaInicio` DATE NOT NULL,
  `fechatermino` DATE NOT NULL,
  PRIMARY KEY (`idCastigado`),
  UNIQUE INDEX `idtbl_castigado_UNIQUE` (`idCastigado` ASC) VISIBLE,
  INDEX `fk_tbl_castigado_tbl_cliente1_idx` (`idCliente` ASC) VISIBLE,
  CONSTRAINT `fk_tbl_castigado_tbl_cliente1`
    FOREIGN KEY (`idCliente`)
    REFERENCES `biblioteca`.`tbl_cliente` (`idCliente`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
