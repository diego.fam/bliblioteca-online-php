-- --------------------------------------------------------
-- Host:                         localhost
-- Versión del servidor:         5.7.19 - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura para tabla biblioteca.tbl_autores
CREATE TABLE IF NOT EXISTS `tbl_autores` (
  `idAutor` int(11) NOT NULL AUTO_INCREMENT,
  `idPersona` int(11) NOT NULL,
  `fechaCreacion` date NOT NULL,
  `estado` tinyint(4) NOT NULL,
  PRIMARY KEY (`idAutor`),
  UNIQUE KEY `idtbl_autores_UNIQUE` (`idAutor`),
  KEY `fk_tbl_autores_tbl_personas1_idx` (`idPersona`),
  CONSTRAINT `fk_tbl_autores_tbl_personas1` FOREIGN KEY (`idPersona`) REFERENCES `tbl_personas` (`idPersona`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin2;

-- Volcando datos para la tabla biblioteca.tbl_autores: ~0 rows (aproximadamente)
DELETE FROM `tbl_autores`;
/*!40000 ALTER TABLE `tbl_autores` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_autores` ENABLE KEYS */;

-- Volcando estructura para tabla biblioteca.tbl_categoria_libro
CREATE TABLE IF NOT EXISTS `tbl_categoria_libro` (
  `idCategoriaLibro` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  PRIMARY KEY (`idCategoriaLibro`),
  UNIQUE KEY `idtbl_categoria_libro_UNIQUE` (`idCategoriaLibro`)
) ENGINE=InnoDB DEFAULT CHARSET=latin2;

-- Volcando datos para la tabla biblioteca.tbl_categoria_libro: ~0 rows (aproximadamente)
DELETE FROM `tbl_categoria_libro`;
/*!40000 ALTER TABLE `tbl_categoria_libro` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_categoria_libro` ENABLE KEYS */;

-- Volcando estructura para tabla biblioteca.tbl_ciudad
CREATE TABLE IF NOT EXISTS `tbl_ciudad` (
  `idCiudad` int(11) NOT NULL AUTO_INCREMENT,
  `idPais` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  PRIMARY KEY (`idCiudad`),
  UNIQUE KEY `idtbl_ciudad_UNIQUE` (`idCiudad`),
  KEY `fk_tbl_ciudad_tbl_pais1_idx` (`idPais`),
  CONSTRAINT `fk_tbl_ciudad_tbl_pais1` FOREIGN KEY (`idPais`) REFERENCES `tbl_pais` (`idPais`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin2;

-- Volcando datos para la tabla biblioteca.tbl_ciudad: ~0 rows (aproximadamente)
DELETE FROM `tbl_ciudad`;
/*!40000 ALTER TABLE `tbl_ciudad` DISABLE KEYS */;
INSERT INTO `tbl_ciudad` (`idCiudad`, `idPais`, `nombre`) VALUES
	(1, 1, 'Villa alemana');
/*!40000 ALTER TABLE `tbl_ciudad` ENABLE KEYS */;

-- Volcando estructura para tabla biblioteca.tbl_cliente
CREATE TABLE IF NOT EXISTS `tbl_cliente` (
  `idCliente` int(11) NOT NULL AUTO_INCREMENT,
  `idPersona` int(11) DEFAULT NULL,
  `idUsuario` int(11) DEFAULT NULL,
  `fechaCreacion` date NOT NULL,
  `estado` tinyint(4) NOT NULL,
  PRIMARY KEY (`idCliente`),
  UNIQUE KEY `idCliente_UNIQUE` (`idCliente`),
  KEY `fk_tbl_cliente_tbl_personas1_idx` (`idPersona`),
  KEY `fk_tbl_cliente_tbl_usuario1_idx` (`idUsuario`),
  CONSTRAINT `fk_tbl_cliente_tbl_personas1` FOREIGN KEY (`idPersona`) REFERENCES `tbl_personas` (`idPersona`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_cliente_tbl_usuario1` FOREIGN KEY (`idUsuario`) REFERENCES `tbl_usuario` (`idUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin2;

-- Volcando datos para la tabla biblioteca.tbl_cliente: ~0 rows (aproximadamente)
DELETE FROM `tbl_cliente`;
/*!40000 ALTER TABLE `tbl_cliente` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_cliente` ENABLE KEYS */;

-- Volcando estructura para tabla biblioteca.tbl_comprobanteprestamo
CREATE TABLE IF NOT EXISTS `tbl_comprobanteprestamo` (
  `idComprobantePrestamo` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` datetime NOT NULL,
  `estado` tinyint(4) NOT NULL,
  `tbl_cliente_idCliente` int(11) NOT NULL,
  PRIMARY KEY (`idComprobantePrestamo`),
  UNIQUE KEY `idtbl_comprobantePrestamo_UNIQUE` (`idComprobantePrestamo`),
  KEY `fk_tbl_comprobantePrestamo_tbl_cliente1_idx` (`tbl_cliente_idCliente`),
  CONSTRAINT `fk_tbl_comprobantePrestamo_tbl_cliente1` FOREIGN KEY (`tbl_cliente_idCliente`) REFERENCES `tbl_cliente` (`idCliente`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin2;

-- Volcando datos para la tabla biblioteca.tbl_comprobanteprestamo: ~0 rows (aproximadamente)
DELETE FROM `tbl_comprobanteprestamo`;
/*!40000 ALTER TABLE `tbl_comprobanteprestamo` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_comprobanteprestamo` ENABLE KEYS */;

-- Volcando estructura para tabla biblioteca.tbl_comprobanteventa
CREATE TABLE IF NOT EXISTS `tbl_comprobanteventa` (
  `idComprobanteVenta` int(11) NOT NULL AUTO_INCREMENT,
  `idCliente` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `estado` tinyint(4) NOT NULL,
  PRIMARY KEY (`idComprobanteVenta`),
  UNIQUE KEY `idtbl_comprobanteCompra_UNIQUE` (`idComprobanteVenta`),
  KEY `fk_tbl_comprobanteCompra_tbl_cliente1_idx` (`idCliente`),
  CONSTRAINT `fk_tbl_comprobanteCompra_tbl_cliente1` FOREIGN KEY (`idCliente`) REFERENCES `tbl_cliente` (`idCliente`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin2;

-- Volcando datos para la tabla biblioteca.tbl_comprobanteventa: ~0 rows (aproximadamente)
DELETE FROM `tbl_comprobanteventa`;
/*!40000 ALTER TABLE `tbl_comprobanteventa` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_comprobanteventa` ENABLE KEYS */;

-- Volcando estructura para tabla biblioteca.tbl_descuento
CREATE TABLE IF NOT EXISTS `tbl_descuento` (
  `idDescuento` int(11) NOT NULL AUTO_INCREMENT,
  `detalle` varchar(50) NOT NULL,
  PRIMARY KEY (`idDescuento`),
  UNIQUE KEY `idtbl_descuento_UNIQUE` (`idDescuento`)
) ENGINE=InnoDB DEFAULT CHARSET=latin2;

-- Volcando datos para la tabla biblioteca.tbl_descuento: ~0 rows (aproximadamente)
DELETE FROM `tbl_descuento`;
/*!40000 ALTER TABLE `tbl_descuento` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_descuento` ENABLE KEYS */;

-- Volcando estructura para tabla biblioteca.tbl_editorial
CREATE TABLE IF NOT EXISTS `tbl_editorial` (
  `idEditorial` int(11) NOT NULL AUTO_INCREMENT,
  `idCiudad` int(11) NOT NULL,
  `correoElectronico` varchar(70) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `estado` tinyint(4) NOT NULL,
  PRIMARY KEY (`idEditorial`),
  UNIQUE KEY `idtbl_editorial_UNIQUE` (`idEditorial`),
  KEY `fk_tbl_editorial_tbl_ciudad1_idx` (`idCiudad`),
  CONSTRAINT `fk_tbl_editorial_tbl_ciudad1` FOREIGN KEY (`idCiudad`) REFERENCES `tbl_ciudad` (`idCiudad`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin2;

-- Volcando datos para la tabla biblioteca.tbl_editorial: ~0 rows (aproximadamente)
DELETE FROM `tbl_editorial`;
/*!40000 ALTER TABLE `tbl_editorial` DISABLE KEYS */;
INSERT INTO `tbl_editorial` (`idEditorial`, `idCiudad`, `correoElectronico`, `nombre`, `estado`) VALUES
	(1, 1, '111@11.cl', 'diego prueba1', 0),
	(2, 1, 'diego@diego.cl', 'Falso 123', 0);
/*!40000 ALTER TABLE `tbl_editorial` ENABLE KEYS */;

-- Volcando estructura para tabla biblioteca.tbl_estante
CREATE TABLE IF NOT EXISTS `tbl_estante` (
  `idEstante` int(11) NOT NULL AUTO_INCREMENT,
  `idPasillo` int(11) NOT NULL,
  `idSectorEstante` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  PRIMARY KEY (`idEstante`),
  UNIQUE KEY `idtbl_estante_UNIQUE` (`idEstante`),
  KEY `fk_tbl_estante_tbl_pasillo1_idx` (`idPasillo`),
  KEY `fk_tbl_estante_tbl_sectorEstante1_idx` (`idSectorEstante`),
  CONSTRAINT `fk_tbl_estante_tbl_pasillo1` FOREIGN KEY (`idPasillo`) REFERENCES `tbl_pasillo` (`idPasillo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_estante_tbl_sectorEstante1` FOREIGN KEY (`idSectorEstante`) REFERENCES `tbl_sectorestante` (`idSectorEstante`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin2;

-- Volcando datos para la tabla biblioteca.tbl_estante: ~0 rows (aproximadamente)
DELETE FROM `tbl_estante`;
/*!40000 ALTER TABLE `tbl_estante` DISABLE KEYS */;
INSERT INTO `tbl_estante` (`idEstante`, `idPasillo`, `idSectorEstante`, `nombre`) VALUES
	(2, 1, 4, 'Ficcion1'),
	(3, 1, 4, 'Amor');
/*!40000 ALTER TABLE `tbl_estante` ENABLE KEYS */;

-- Volcando estructura para tabla biblioteca.tbl_facturacion
CREATE TABLE IF NOT EXISTS `tbl_facturacion` (
  `idFacturacion` int(11) NOT NULL AUTO_INCREMENT,
  `electronico` varchar(45) NOT NULL,
  PRIMARY KEY (`idFacturacion`),
  UNIQUE KEY `idtbl_facturacion_UNIQUE` (`idFacturacion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin2;

-- Volcando datos para la tabla biblioteca.tbl_facturacion: ~0 rows (aproximadamente)
DELETE FROM `tbl_facturacion`;
/*!40000 ALTER TABLE `tbl_facturacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_facturacion` ENABLE KEYS */;

-- Volcando estructura para tabla biblioteca.tbl_libros
CREATE TABLE IF NOT EXISTS `tbl_libros` (
  `idLibro` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idCategoriaLibro` int(11) NOT NULL,
  `idAutor` int(11) NOT NULL,
  `idEditorial` int(11) NOT NULL,
  `idProveedor` int(11) NOT NULL,
  `idTipoLibro` int(11) NOT NULL,
  `idEstante` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `codigo` varchar(50) NOT NULL,
  `stockPrestamo` int(12) DEFAULT NULL,
  `stockVenta` int(12) DEFAULT NULL,
  `valorPrestano` int(11) NOT NULL,
  `valorVenta` int(11) NOT NULL,
  `resumen` longtext NOT NULL,
  `estado` tinyint(4) NOT NULL,
  `imagen` blob,
  `fechaIngreso` date NOT NULL,
  `descripcion` longtext NOT NULL,
  PRIMARY KEY (`idLibro`),
  KEY `fk_tbl_libros_tbl_categoria_libro1_idx` (`idCategoriaLibro`),
  KEY `fk_tbl_libros_tbl_autores1_idx` (`idAutor`),
  KEY `fk_tbl_libros_tbl_editorial1_idx` (`idEditorial`),
  KEY `fk_tbl_libros_tbl_proveedor1_idx` (`idProveedor`),
  KEY `fk_tbl_libros_tbl_tipoLibro1_idx` (`idTipoLibro`),
  KEY `fk_tbl_libros_tbl_estante1_idx` (`idEstante`),
  CONSTRAINT `fk_tbl_libros_tbl_autores1` FOREIGN KEY (`idAutor`) REFERENCES `tbl_autores` (`idAutor`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_libros_tbl_categoria_libro1` FOREIGN KEY (`idCategoriaLibro`) REFERENCES `tbl_categoria_libro` (`idCategoriaLibro`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_libros_tbl_editorial1` FOREIGN KEY (`idEditorial`) REFERENCES `tbl_editorial` (`idEditorial`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_libros_tbl_estante1` FOREIGN KEY (`idEstante`) REFERENCES `tbl_estante` (`idEstante`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_libros_tbl_proveedor1` FOREIGN KEY (`idProveedor`) REFERENCES `tbl_proveedor` (`idProveedor`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_libros_tbl_tipoLibro1` FOREIGN KEY (`idTipoLibro`) REFERENCES `tbl_tipolibro` (`idTipoLibro`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin2;

-- Volcando datos para la tabla biblioteca.tbl_libros: ~0 rows (aproximadamente)
DELETE FROM `tbl_libros`;
/*!40000 ALTER TABLE `tbl_libros` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_libros` ENABLE KEYS */;

-- Volcando estructura para tabla biblioteca.tbl_pago
CREATE TABLE IF NOT EXISTS `tbl_pago` (
  `idPago` int(11) NOT NULL AUTO_INCREMENT,
  `idTipoPago` int(10) unsigned NOT NULL,
  `idDescuento` int(11) NOT NULL,
  `idFacturacion` int(11) NOT NULL,
  `idComprobanteVenta` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `neto` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  PRIMARY KEY (`idPago`),
  UNIQUE KEY `idtbl_pago_UNIQUE` (`idPago`),
  KEY `fk_tbl_pago_tbl_tipoPago1_idx` (`idTipoPago`),
  KEY `fk_tbl_pago_tbl_descuento1_idx` (`idDescuento`),
  KEY `fk_tbl_pago_tbl_facturacion1_idx` (`idFacturacion`),
  KEY `fk_tbl_pago_tbl_comprobanteVenta1_idx` (`idComprobanteVenta`),
  CONSTRAINT `fk_tbl_pago_tbl_comprobanteVenta1` FOREIGN KEY (`idComprobanteVenta`) REFERENCES `tbl_comprobanteventa` (`idComprobanteVenta`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_pago_tbl_descuento1` FOREIGN KEY (`idDescuento`) REFERENCES `tbl_descuento` (`idDescuento`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_pago_tbl_facturacion1` FOREIGN KEY (`idFacturacion`) REFERENCES `tbl_facturacion` (`idFacturacion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_pago_tbl_tipoPago1` FOREIGN KEY (`idTipoPago`) REFERENCES `tbl_tipopago` (`idTipoPago`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin2;

-- Volcando datos para la tabla biblioteca.tbl_pago: ~0 rows (aproximadamente)
DELETE FROM `tbl_pago`;
/*!40000 ALTER TABLE `tbl_pago` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_pago` ENABLE KEYS */;

-- Volcando estructura para tabla biblioteca.tbl_pais
CREATE TABLE IF NOT EXISTS `tbl_pais` (
  `idPais` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  PRIMARY KEY (`idPais`),
  UNIQUE KEY `idPais_UNIQUE` (`idPais`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin2;

-- Volcando datos para la tabla biblioteca.tbl_pais: ~0 rows (aproximadamente)
DELETE FROM `tbl_pais`;
/*!40000 ALTER TABLE `tbl_pais` DISABLE KEYS */;
INSERT INTO `tbl_pais` (`idPais`, `nombre`) VALUES
	(1, 'Chile');
/*!40000 ALTER TABLE `tbl_pais` ENABLE KEYS */;

-- Volcando estructura para tabla biblioteca.tbl_pasillo
CREATE TABLE IF NOT EXISTS `tbl_pasillo` (
  `idPasillo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  PRIMARY KEY (`idPasillo`),
  UNIQUE KEY `idtbl_pasillo_UNIQUE` (`idPasillo`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin2;

-- Volcando datos para la tabla biblioteca.tbl_pasillo: ~0 rows (aproximadamente)
DELETE FROM `tbl_pasillo`;
/*!40000 ALTER TABLE `tbl_pasillo` DISABLE KEYS */;
INSERT INTO `tbl_pasillo` (`idPasillo`, `nombre`) VALUES
	(1, 'A');
/*!40000 ALTER TABLE `tbl_pasillo` ENABLE KEYS */;

-- Volcando estructura para tabla biblioteca.tbl_personas
CREATE TABLE IF NOT EXISTS `tbl_personas` (
  `idPersona` int(11) NOT NULL AUTO_INCREMENT,
  `idCiudad` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellidoPaterno` varchar(50) NOT NULL,
  `apellidoMaterno` varchar(50) NOT NULL,
  `correoElectronico` varchar(70) NOT NULL,
  `cedula` varchar(13) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `estado` tinyint(4) NOT NULL,
  PRIMARY KEY (`idPersona`),
  UNIQUE KEY `idtbl_personas_UNIQUE` (`idPersona`),
  KEY `fk_tbl_personas_tbl_ciudad1_idx` (`idCiudad`),
  CONSTRAINT `fk_tbl_personas_tbl_ciudad1` FOREIGN KEY (`idCiudad`) REFERENCES `tbl_ciudad` (`idCiudad`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin2;

-- Volcando datos para la tabla biblioteca.tbl_personas: ~0 rows (aproximadamente)
DELETE FROM `tbl_personas`;
/*!40000 ALTER TABLE `tbl_personas` DISABLE KEYS */;
INSERT INTO `tbl_personas` (`idPersona`, `idCiudad`, `nombre`, `apellidoPaterno`, `apellidoMaterno`, `correoElectronico`, `cedula`, `direccion`, `estado`) VALUES
	(1, 1, 'Diego', 'Alfaro', 'Molina', 'diego@diego.cl', '11.111.111-8', 'Calle Falsa 123', 0);
/*!40000 ALTER TABLE `tbl_personas` ENABLE KEYS */;

-- Volcando estructura para tabla biblioteca.tbl_proveedor
CREATE TABLE IF NOT EXISTS `tbl_proveedor` (
  `idProveedor` int(11) NOT NULL AUTO_INCREMENT,
  `idCiudad` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `estado` tinyint(4) NOT NULL,
  PRIMARY KEY (`idProveedor`),
  UNIQUE KEY `idtbl_proveedor_UNIQUE` (`idProveedor`),
  KEY `fk_tbl_proveedor_tbl_ciudad1_idx` (`idCiudad`),
  CONSTRAINT `fk_tbl_proveedor_tbl_ciudad1` FOREIGN KEY (`idCiudad`) REFERENCES `tbl_ciudad` (`idCiudad`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin2;

-- Volcando datos para la tabla biblioteca.tbl_proveedor: ~0 rows (aproximadamente)
DELETE FROM `tbl_proveedor`;
/*!40000 ALTER TABLE `tbl_proveedor` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_proveedor` ENABLE KEYS */;

-- Volcando estructura para tabla biblioteca.tbl_sectorestante
CREATE TABLE IF NOT EXISTS `tbl_sectorestante` (
  `idSectorEstante` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  PRIMARY KEY (`idSectorEstante`),
  UNIQUE KEY `idtbl_ubicacionLibro_UNIQUE` (`idSectorEstante`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin2;

-- Volcando datos para la tabla biblioteca.tbl_sectorestante: ~0 rows (aproximadamente)
DELETE FROM `tbl_sectorestante`;
/*!40000 ALTER TABLE `tbl_sectorestante` DISABLE KEYS */;
INSERT INTO `tbl_sectorestante` (`idSectorEstante`, `nombre`) VALUES
	(1, 'Plata Roja1'),
	(4, 'Ventana');
/*!40000 ALTER TABLE `tbl_sectorestante` ENABLE KEYS */;

-- Volcando estructura para tabla biblioteca.tbl_tipolibro
CREATE TABLE IF NOT EXISTS `tbl_tipolibro` (
  `idTipoLibro` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  PRIMARY KEY (`idTipoLibro`),
  UNIQUE KEY `idtbl_tipoLibro_UNIQUE` (`idTipoLibro`)
) ENGINE=InnoDB DEFAULT CHARSET=latin2;

-- Volcando datos para la tabla biblioteca.tbl_tipolibro: ~0 rows (aproximadamente)
DELETE FROM `tbl_tipolibro`;
/*!40000 ALTER TABLE `tbl_tipolibro` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_tipolibro` ENABLE KEYS */;

-- Volcando estructura para tabla biblioteca.tbl_tipopago
CREATE TABLE IF NOT EXISTS `tbl_tipopago` (
  `idTipoPago` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `detalle` varchar(50) NOT NULL,
  PRIMARY KEY (`idTipoPago`),
  UNIQUE KEY `idtbl_tipopago_UNIQUE` (`idTipoPago`)
) ENGINE=InnoDB DEFAULT CHARSET=latin2;

-- Volcando datos para la tabla biblioteca.tbl_tipopago: ~0 rows (aproximadamente)
DELETE FROM `tbl_tipopago`;
/*!40000 ALTER TABLE `tbl_tipopago` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_tipopago` ENABLE KEYS */;

-- Volcando estructura para tabla biblioteca.tbl_tipo_usuarios
CREATE TABLE IF NOT EXISTS `tbl_tipo_usuarios` (
  `idTipoUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombreTipoUsuario` varchar(50) NOT NULL,
  PRIMARY KEY (`idTipoUsuario`),
  UNIQUE KEY `idtbl_tipo_usuarios_UNIQUE` (`idTipoUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin2;

-- Volcando datos para la tabla biblioteca.tbl_tipo_usuarios: ~0 rows (aproximadamente)
DELETE FROM `tbl_tipo_usuarios`;
/*!40000 ALTER TABLE `tbl_tipo_usuarios` DISABLE KEYS */;
INSERT INTO `tbl_tipo_usuarios` (`idTipoUsuario`, `nombreTipoUsuario`) VALUES
	(1, 'usuario'),
	(2, 'administrador');
/*!40000 ALTER TABLE `tbl_tipo_usuarios` ENABLE KEYS */;

-- Volcando estructura para tabla biblioteca.tbl_usuario
CREATE TABLE IF NOT EXISTS `tbl_usuario` (
  `idUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `idTipoUsuario` int(11) NOT NULL,
  `idPersona` int(11) NOT NULL,
  `fechaCreacion` date NOT NULL,
  `userName` varchar(15) NOT NULL,
  `password` varchar(42) NOT NULL,
  `estado` tinyint(4) NOT NULL,
  PRIMARY KEY (`idUsuario`),
  UNIQUE KEY `idUsuario_UNIQUE` (`idUsuario`),
  UNIQUE KEY `userName_UNIQUE` (`userName`),
  KEY `fk_tbl_usuario_tbl_tipo_usuarios_idx` (`idTipoUsuario`),
  KEY `fk_tbl_usuario_tbl_personas1_idx` (`idPersona`),
  CONSTRAINT `fk_tbl_usuario_tbl_personas1` FOREIGN KEY (`idPersona`) REFERENCES `tbl_personas` (`idPersona`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_usuario_tbl_tipo_usuarios` FOREIGN KEY (`idTipoUsuario`) REFERENCES `tbl_tipo_usuarios` (`idTipoUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin2;

-- Volcando datos para la tabla biblioteca.tbl_usuario: ~0 rows (aproximadamente)
DELETE FROM `tbl_usuario`;
/*!40000 ALTER TABLE `tbl_usuario` DISABLE KEYS */;
INSERT INTO `tbl_usuario` (`idUsuario`, `idTipoUsuario`, `idPersona`, `fechaCreacion`, `userName`, `password`, `estado`) VALUES
	(1, 2, 1, '2019-01-17', 'dalfaro', 'diego', 0);
/*!40000 ALTER TABLE `tbl_usuario` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
