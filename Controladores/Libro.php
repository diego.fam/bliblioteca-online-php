<?php
class Libro
{
    public function __construct(){
    }

    public function index()
    {

       $conexion= new mysqli("localhost","root","","biblioteca");
        mysqli_set_charset($conexion, "utf8");

       $query="SELECT * FROM tbl_libros";

        $libros = mysqli_query($conexion, $query);


        include ('Vistas/Libro/libroCrud.php');

    }

    public function agregar()
    {
        $conexion= new mysqli("localhost","root","","biblioteca");
        mysqli_set_charset($conexion, "utf8");

        $queryCategorias = "SELECT * FROM tbl_categoria_libro";
        $categorias1 = mysqli_query($conexion, $queryCategorias);

        $queryAutores = "SELECT * FROM tbl_autores";
        $autores = mysqli_query($conexion, $queryAutores);
       
        $queryEditoriales = "SELECT * FROM tbl_editorial";
        $editoriales = mysqli_query($conexion, $queryEditoriales);

        $queryTipo = "SELECT * FROM tbl_tipoLibro";
        $tiposLibros = mysqli_query($conexion, $queryTipo);

        $queryEstantes = "SELECT * FROM tbl_estante";
        $estantes = mysqli_query($conexion, $queryEstantes);

        include ('Vistas/Libro/libroNuevo.php');
    }

    public function guardar()
    {
        
        $categoria = $_POST['categoria'];
        $autor = $_POST['autor'];
        $editorial = $_POST['editorial'];
        $tipoLibro = $_POST['tipolibro'];
        $estante = $_POST['estante'];
        $nombre = $_POST['nombre'];
        $codigo = $_POST['codigo'];
        $stockP = $_POST['stockP'];
        $stockV = $_POST['stockV'];
        $valorP = $_POST['valorP'];
        $valorV = $_POST['valorV'];
        $resumen = $_POST['resumen'];
        $imagen = $_FILES['imagen'];
        $proveedor = 1;
        $estado = 0;
        $fecha = date("Y-m-d");

        $conexion= new mysqli("localhost","root","","biblioteca");
        mysqli_set_charset($conexion, "utf8");

       $query="INSERT INTO tbl_libros (idCategoriaLibro, idAutor, idEditorial, idProveedor, idTipoLibro, idEstante, nombre, codigo,
                            stockPrestamo, stockVenta, resumen, estado, imagen, fechaIngreso, descripcion, valorPrestano, valorVenta)
                 VALUES ($categoria, $autor, $editorial, $proveedor, $tipoLibro, $estante, '$nombre', '$codigo', $stockP, $stockV, '$resumen', $estado,
                    '$imagen', '$fecha', '$resumen', $valorP, $valorV)";

        $resultadoInsert = mysqli_query($conexion, $query);

        header('Location: index.php?controlador=Libro&funcion=index');

    }
}
?>