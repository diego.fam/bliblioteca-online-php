<?php
    class Inicio
    {
        public function __construct(){
        }

        public function index()
        {
 
            $conexion= new mysqli("localhost","root","","biblioteca");
            mysqli_set_charset($conexion, "utf8");

            $query = "SELECT DISTINCT nombre, fechaIngreso, idLibro,imagen FROM `tbl_libros` ORDER BY fechaIngreso DESC limit 5";

            $resultadoInicio =  mysqli_query($conexion, $query);

            $queryAleatoria = "SELECT DISTINCT nombre, fechaIngreso, idLibro,imagen FROM `tbl_libros` ORDER BY RAND() LIMIT 3";

            $resultadoAleatorio = mysqli_query($conexion, $queryAleatoria);
            
            include('Vistas/inicio.php');

            
        }
    }
?>