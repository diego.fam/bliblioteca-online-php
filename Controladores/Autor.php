<?php
class Autor
{
    public function __construct(){
    }

    public function index()
    {

        $conexion= new mysqli("localhost","root","","biblioteca");
        mysqli_set_charset($conexion, "utf8");

        $query = "SELECT * FROM tbl_autores, tbl_personas WHERE tbl_autores.idPersona = tbl_personas.idPersona";


        $autores =  mysqli_query($conexion, $query);

        include('Vistas/Autor/autorCrud.php');

    }

    public function agregar()
    {
        $conexion= new mysqli("localhost","root","","biblioteca");
        mysqli_set_charset($conexion, "utf8");

        $query = "SELECT * FROM tbl_personas";


        $personas =  mysqli_query($conexion, $query);

        include('Vistas/Autor/autorNuevo.php');
    }

    public function guardar()
    {

        $persona = $_POST['persona'];
        $fecha = date("Y-m-d");
        $estado = 0;

        $conexion= new mysqli("localhost","root","","biblioteca");
        mysqli_set_charset($conexion, "utf8");

        $query = "INSERT INTO tbl_autores (idPersona, fechaCreacion, estado)
                    VALUES ($persona, '$fecha', $estado)";


        $autores =  mysqli_query($conexion, $query);

        header('Location: index.php?controlador=Autor&funcion=index');
    }

    public function eliminar()
    {
        $autor = $_POST['eliminar'];

        $conexion= new mysqli("localhost","root","","biblioteca");
        mysqli_set_charset($conexion, "utf8");

        $query = "DELETE FROM tbl_autores WHERE idAutor = $autor";


        $autores =  mysqli_query($conexion, $query);

        header('Location: index.php?controlador=Autor&funcion=index');
    }
}
?>