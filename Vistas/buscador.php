<?php

include('Layout/header.php');
//include('Layout/content.php');
?>


    <div class="contenido">

        <h1>Resultados de su busqueda</h1>

        <table>
            <tbody>
            <tr>
                <?php
                $i = 0;
                while($render = mysqli_fetch_array($resultadoBusqueda)){
                    $i++;
                ?>

                    <td class="libroContenedor">
                        <div class="libroContenedorPortada">
                            <?php echo '<a href="index.php?controlador=detalleLibro&funcion=index&libro='.$render['idLibro'].'"><img class="portadaLibro" src="data:image/jpeg;base64,'.base64_encode( $render['imagen'] ).'"/></a>'; ?>
                        </div>
                        <div class="libroContenedorTitulo">
                            <?php echo $render['nombre'] ?>
                        </div>
                        <div class="libroContenedorFecha">
                            <?php $date = new DateTime ($render['fechaIngreso']) ?>
                            <?php echo $date->format('d-m-Y'); ?>
                        </div>
                    </td>
                    <?php
                    /*if($i == 5){
                        echo '</tr>';
                        echo '<tr>';
                    }*/
                    switch ($i){
                        case 5:
                            echo '</tr>';
                            echo '<tr>';
                        break;

                        case 10:
                            echo '</tr>';
                            echo '<tr>';
                        break;

                        case 15:
                            echo '</tr>';
                            echo '<tr>';
                        break;

                        case 20:
                            echo '</tr>';
                            echo '<tr>';
                        break;
                    }

                    ?>
                <?php
                }
                ?>
            </tr>
            </tbody>
        </table>

    </div>
