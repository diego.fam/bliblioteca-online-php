<?php
include('Vistas/Layout/header.php');
?>
<div class= "contenido" >
<center><h1>Modificar Persona</h1></center>
<center>
<form action="index.php?controlador=Admin&funcion=index" method="POST">
        <input type="submit" name="" value="Inicio Administracion"/>
    </form>
<br>
<form action="index.php?controlador=Persona&funcion=guardarEditar" method="POST">

    <input type="hidden" name="idpersona" value="<?php echo $persona['idPersona'] ?>">

    <label for="nombre">Nombre</label>
    <input type="text" name="nombre" id="nombre" value="<?php echo $persona['nombre'] ?>" placeholder="Primer nombre" required>&nbsp;&nbsp;

    <label for="apellidoPaterno">Apellido Paterno</label>
    <input type="text" name="apellidoPaterno" id="apellidoPaterno" value="<?php echo $persona['apellidoPaterno'] ?>"  placeholder="Apellido Paterno" required>&nbsp;&nbsp;

    <label for="apellidoMaterno">Apellido Paterno</label>
    <input type="text" name="apellidoMaterno" id="apellidoMaterno" value="<?php echo $persona['apellidoMaterno'] ?>" placeholder="Apellido Materno" required>&nbsp;&nbsp;

    <label for="ciudad">Ciudad</label>
    <select name="ciudad" id="ciudad" required>
        <?php
            while($ciudad = mysqli_fetch_array($ciudades)){
        ?>
            <option value="<?php echo $ciudad['idCiudad'] ?>"><?php echo $ciudad['nombre'] ?></option>
        <?php
            }
        ?>
    </select>
    <br>
    <br>
    <br>
    <label for="email">Email</label>
    <input type="email" required name="email" id="email" value="<?php echo $persona['correoElectronico'] ?>" placeholder="Email">&nbsp;&nbsp;
    
    <label for="cedula">Cedula de Identidad</label>
    <input type="text" required name="cedula" id="cedula" value="<?php echo $persona['cedula'] ?>" placeholder="Con puntos y con guion">&nbsp;&nbsp;

    <label for="direccion">Direccion</label>
    <input type="text" required name="direccion" id="direccion" value="<?php echo $persona['direccion'] ?>" placeholder="Calle y numero">&nbsp;&nbsp;
    <br>
    <br>
    <br>
    <input type="submit" name="ingresar" value="INGRESAR"/>
</form>
</center>
</div>
