<?php
include('Vistas/Layout/header.php');
?>
<div class= "contenido" >
<center><h1>Nuevo Libro</h1></center>
<center>
<form action="index.php?controlador=Admin&funcion=index" method="POST">
        <input type="submit" name="" value="Inicio Administracion"/>
    </form>
<br>
<form action="index.php?controlador=Libro&funcion=guardar" enctype="multipart/form-data" method="POST">

<label for="categoria">Categoria Libro</label>
    <select name="categoria" id="categoria" required>
        <?php
            while($categoria = mysqli_fetch_array($categorias1)){
        ?>
            <option value="<?php echo $categoria['idCategoriaLibro'] ?>"><?php echo $categoria['nombre'] ?></option>
        <?php
            }
        ?>
    </select>

    <label for="autor">Autor</label>
    <select name="autor" id="autor" required>
        <?php
            while($autor = mysqli_fetch_array($autores)){
        ?>
            <option value="<?php echo $autor['idAutor'] ?>"><?php echo $autor['idPersona'] ?></option>
        <?php
            }
        ?>
    </select>
    <br>
    <br>
    <br>
    <label for="editorial">Editorial</label>
    <select name="editorial" id="editorial" required>
        <?php
            while($editorial = mysqli_fetch_array($editoriales)){
        ?>
            <option value="<?php echo $editorial['idEditorial'] ?>"><?php echo $editorial['nombre'] ?></option>
        <?php
            }
        ?>
    </select>

    <label for="tipolibro">Tipo de Libro</label>
    <select name="tipolibro" id="tipolibro" required>
        <?php
            while($tipolibro = mysqli_fetch_array($tiposLibros)){
        ?>
            <option value="<?php echo $tipolibro['idTipoLibro'] ?>"><?php echo $tipolibro['nombre'] ?></option>
        <?php
            }
        ?>
    </select>

    <label for="estante">Estante</label>
    <select name="estante" id="estante" required>
        <?php
            while($estante = mysqli_fetch_array($estantes)){
        ?>
            <option value="<?php echo $estante['idEstante'] ?>"><?php echo $estante['nombre'] ?></option>
        <?php
            }
        ?>
    </select>
    <br>
    <br>
    <br>
            <label for="nombre">Nombre Libro</label>
            <input type="text" name="nombre" id="nombre">

            <label for="codigo">Codigo Libro</label>
            <input type="text" name="codigo" id="codigo">

            <label for="stockP">Stock Prestamo</label>
            <input type="number" name="stockP" id="stockP">
            <br>
            <br>
            <label for="stockV">Stock Venta</label>
            <input type="number" name="stockV" id="stockV">

            <label for="valorP">Valor Prestamo</label>
            <input type="number" name="valorP" id="valorP">

            <label for="valorV">Valor Venta</label>
            <input type="number" name="valorV" id="valorV">
            <br>
            <br>
            <label for="resumen">Resumen y/o Descripcion</label>
            <textarea name="resumen" id="resumen"> </textarea>
            <br>
            <br>
            <label for="imagen">Portada</label>
            <input type="file" name="imagen" id="imagen">
            <br>
            <br>
    <input type="submit" name="ingresar" value="INGRESAR"/>
</form>
</center>
</div>
