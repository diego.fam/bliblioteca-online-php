<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Biblioteca Virtual</title>

	<!-- Assets -->

	<link rel="stylesheet" href="Assets/css/estilos.css">
	<script src="Assets/js/js.js"></script>
	<link rel="stylesheet" href="Assets/css/all.css">
	<script src="Assets/js/jquery.js"></script>
	<link rel="stylesheet" href="Assets/dataTables/datatables.min.css">
	<script src="Assets/dataTables/datatables.min.js"></script>
	<!-- Fin Assets -->

</head>
<body>
<header>
	<?php
	$conexion= new mysqli("localhost","root","","biblioteca");
	mysqli_set_charset($conexion, "utf8");

	$query = "SELECT idCategoriaLibro, nombre FROM `tbl_categoria_libro`";

	$categorias =  mysqli_query($conexion, $query);
	?>
<div class="header">

	<div class="logo">
		<a href="/"><img class="imagenes" src="./Assets/imgs/logo.png"></a>
	</div>

	<div class="buscador">

	<form action="index.php?controlador=Buscador&funcion=index" method="post">
		<select class="selectBuscador" name="categoriaLibro">
            <option value="0">Todos</option>
		<?php
                    while($categoria = mysqli_fetch_array($categorias)){
                
						echo "<option value='".$categoria['idCategoriaLibro']."'>".$categoria['nombre']."</option>";
                    }
        ?>
		</select><!--

	--><input class="inputBuscador" type="text" name="buscador" placeholder="Busca tu libro preferido!"><!--

	--><input class="enviarBuscador" type="submit" value="Buscar">
	</form>
	
	</div>
	<div class="menu" style="font-size: 3rem;">
        <a href="/"><i class="fas fa-home"></i></a>
		<?php if( !isset($_SESSION['idusuario'])){ ?>
			<a href="index.php?controlador=Login&funcion=index"><i class="fas fa-sign-in-alt"></i></a>
			<a href="#"><i class="fas fa-user-plus"></i></a>
		<?php } ?>
		<?php if( isset($_SESSION['idusuario'])){ ?>
			<a href="index.php?controlador=Login&funcion=logout"><i class="fas fa-door-open"></i></a>
			<a href="index.php?controlador=Admin&funcion=index"><i class="fas fa-tools"></i></a>
		<?php } ?>
	</div>
</div>
</header>
