<?php

    include('Layout/header.php');
    //include('Layout/content.php');
?>


<div class="contenido">

    <h1>Ultimos Libros Agregados</h1>

    <table>
        <tbody>
            <tr>
                <?php
                    while($render = mysqli_fetch_array($resultadoInicio)){
                ?>

                <td class="libroContenedor">
                    <div class="libroContenedorPortada">
                        <?php echo '<a href="index.php?controlador=detalleLibro&funcion=index&libro='.$render['idLibro'].'"><img class="portadaLibro" src="data:image/jpeg;base64,'.base64_encode( $render['imagen'] ).'"/></a>'; ?>
                    </div>
                    <div class="libroContenedorTitulo">
                        <?php echo $render['nombre'] ?>
                    </div>
                    <div class="libroContenedorFecha">
                        <?php $date = new DateTime ($render['fechaIngreso']) ?>
                        <?php echo $date->format('d-m-Y'); ?>
                    </div>
                </td>

                <?php
                    }
                ?>
            </tr>
        </tbody>
    </table>

    <h1>Otros Libros Interesantes</h1>

    <table>
        <tbody>
            <tr>
                <?php
                    while($renderAl = mysqli_fetch_array($resultadoAleatorio)){
                ?>

                <td class="libroContenedor">
                    <div class="libroContenedorPortada">
                        <?php echo '<a href="index.php?controlador=detalleLibro&funcion=index&libro='.$renderAl['idLibro'].'"><img class="portadaLibro" src="data:image/jpeg;base64,'.base64_encode( $renderAl['imagen'] ).'"/></a>'; ?>
                    </div>
                    <div class="libroContenedorTitulo">
                        <?php echo $renderAl['nombre'] ?>
                    </div>
                    <div class="libroContenedorFecha">
                        <?php $dateAl = new DateTime ($renderAl['fechaIngreso']) ?>
                        <?php echo $dateAl->format('d-m-Y'); ?>
                    </div>
                </td>

                <?php
                    }
                ?>
            </tr>
        </tbody>
    </table>

</div>
